﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Umbraco.Cms.Web.Editors;
using Umbraco.Cms.Web.Model.BackOffice.Editors;
using Umbraco.Cms.Web.Context;
using Umbraco.Framework;
using Umbraco.Cms.Web.EmbeddedViewEngine;
using Umbraco.Hive;
using Umbraco.Framework.Persistence.Model;
using Umbraco.Framework.Persistence.Model.Attribution;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions;
using Umbraco.Hive.ProviderGrouping;

namespace WebMatters.UmbracoAddOns.MusicCatalogEditors
{
    [Editor("8E71899C-0F8D-11E1-A728-D08B4824019B")]
    public class MusicCatalogArtistEditorController : StandardEditorController
    {
        public MusicCatalogArtistEditorController(IBackOfficeRequestContext requestContext)
            : base(requestContext)
        {
        }

        #region Action methods

        /// <summary>
        /// Renders the create view
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View(EmbeddedViewPath.Create("WebMatters.UmbracoAddOns.MusicCatalogEditors.Views.Create.cshtml,WebMatters.UmbracoAddOns.MusicCatalogEditors"));
        }

        /// <summary>
        /// Accepts the form post from the create view and persists the new artist
        /// </summary>
        /// <param name="formCollection">Form data</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formCollection)
        {
            PersistArtistRecord(new HiveId(HiveIdValue.Empty), formCollection);

            return RedirectToAction("Created");            
        }

        /// <summary>
        /// Helper method to persist the added or edited artist record
        /// </summary>
        /// <param name="hiveId">HiveId of artist</param>
        /// <param name="formCollection">Form data</param>
        private void PersistArtistRecord(HiveId hiveId, FormCollection formCollection)
        {
            // Create TypedEntity from form post
            var entity = new TypedEntity()
            {
                Id = hiveId,
                EntitySchema = new ArtistSchema(),
                UtcCreated = DateTime.Now,
                UtcModified = DateTime.Now,
            };

            // Add the attributes
            var artistName = formCollection["Name"];
            var nameAttribute = new TypedAttribute(new NameAttributeDefinition(), artistName);
            nameAttribute.Values.Add("Name", artistName);
            nameAttribute.Id = new HiveId(nameAttribute.AttributeDefinition.Alias);
            entity.Attributes.Add(nameAttribute);

            var artistDescription = formCollection["Description"];
            var descriptionAttribute = new TypedAttribute(new DescriptionAttributeDefinition(), artistDescription);
            descriptionAttribute.Values.Add("Description", artistDescription);
            descriptionAttribute.Id = new HiveId(descriptionAttribute.AttributeDefinition.Alias);
            entity.Attributes.Add(descriptionAttribute);

            // Get reference to hive and call AddOrUpdate
            var hive = GetHiveWriter();
            using (var uow = hive.Create())
            {
                uow.Repositories.AddOrUpdate<TypedEntity>(new List<TypedEntity> { entity });
            }
        }

        /// <summary>
        /// Renders the "done" view for a created artist
        /// </summary>
        /// <returns></returns>
        public ViewResult Created()
        {
            return View(EmbeddedViewPath.Create("WebMatters.UmbracoAddOns.MusicCatalogEditors.Views.Created.cshtml,WebMatters.UmbracoAddOns.MusicCatalogEditors"));
        }

        /// <summary>
        /// Renders the edit form
        /// </summary>
        /// <param name="id">HiveId of artist to edit</param>
        /// <returns></returns>
        public override ActionResult Edit(HiveId? id)
        {
            if (id.IsNullValueOrEmpty()) return HttpNotFound();

            var hive = GetHiveReader();

            Mandate.That(hive != null, x => new NotSupportedException("Could not find a hive provider for route: " + id.Value.ToString(HiveIdFormatStyle.AsUri)));

            using (var uow = hive.CreateReadonly())
            {
                var artist = uow.Repositories.Get<TypedEntity>(id.Value);
                if (artist != null)
                {
                    return View(EmbeddedViewPath.Create("WebMatters.UmbracoAddOns.MusicCatalogEditors.Views.Edit.cshtml,WebMatters.UmbracoAddOns.MusicCatalogEditors"), artist);
                }
            }

            return HttpNotFound();
        }

        /// <summary>
        /// Accepts the form post from the edit view and persists the edited artist
        /// </summary>
        /// <param name="formCollection">Form data</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection formCollection)
        {
            PersistArtistRecord(new HiveId(formCollection["Id"]), formCollection);

            Notifications.Add(new NotificationMessage("Artist record edited", "Content saved" , NotificationType.Success));

            return RedirectToAction("Edited");            
        }
        
        /// <summary>
        /// Renders the "done" view for an edited artist
        /// </summary>
        /// <returns></returns>
        public ViewResult Edited()
        {
            return View(EmbeddedViewPath.Create("WebMatters.UmbracoAddOns.MusicCatalogEditors.Views.Edited.cshtml,WebMatters.UmbracoAddOns.MusicCatalogEditors"));
        }

        [HttpDelete]
        public JsonResult Delete(HiveId id)
        {
            var hive = GetHiveWriter();
            using (var uow = hive.Create())
            {
                uow.Repositories.Delete<TypedEntity>(id);
            }
            return Json(new { message = "Success" });
        }

        #endregion

        #region Hive helpers

        private ReadonlyGroupUnitFactory<IMusicCatalog> GetHiveReader()
        {
            return BackOfficeRequestContext.Application.Hive.GetReader<IMusicCatalog>(new Uri("webmatters-music-catalog://"));
        }

        private GroupUnitFactory<IMusicCatalog> GetHiveWriter()
        {
            var hive = BackOfficeRequestContext.Application.Hive.GetWriter<IMusicCatalog>(new Uri("webmatters-music-catalog://"));
            return hive;
        }

        #endregion

    }
}