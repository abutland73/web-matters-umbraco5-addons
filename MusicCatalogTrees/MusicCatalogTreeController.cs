﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Umbraco.Cms.Web;
using Umbraco.Cms.Web.Model.BackOffice.Trees;
using Umbraco.Cms.Web.Context;
using Umbraco.Cms.Web.Trees;
using Umbraco.Cms.Web.Trees.MenuItems;
using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model;
using Umbraco.Framework.Persistence.Model.Constants;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity;

namespace WebMatters.UmbracoAddOns.MusicCatalogTrees
{
    [Tree("8af0aade-837b-4be6-9426-842445403bf0", "Music Catalog")]
    public class MusicCatalogTreeController : SupportsEditorTreeController
    {
        public MusicCatalogTreeController(IBackOfficeRequestContext requestContext)
            : base(requestContext)
        {
        }

        protected override HiveId RootNodeId
        {
            get { return FixedHiveIds.SystemRoot; }
        }

        /// <summary>
        /// The Guid identifying the associated editor controller
        /// </summary>
        public override Guid EditorControllerId
        {
            get { return new Guid("8E71899C-0F8D-11E1-A728-D08B4824019B"); }
        }

        /// <summary>
        /// Gets the tree nodes
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="queryStrings"></param>
        /// <returns></returns>
        protected override UmbracoTreeResult GetTreeData(HiveId parentId, FormCollection queryStrings)
        {
            // If its the first level
            if (parentId == RootNodeId)
            {
                // Get hive provider
                var hive = BackOfficeRequestContext.Application.Hive.GetReader<IMusicCatalog>(
                    (parentId == RootNodeId)
                        ? new Uri("webmatters-music-catalog://")
                        : parentId.ToUri());

                Mandate.That(hive != null, x => new NotSupportedException("Could not find a hive provider for route: " + parentId.ToString(HiveIdFormatStyle.AsUri)));

                using (var uow = hive.CreateReadonly())
                {
                    // Get all artists from our hive provider
                    var items = uow.Repositories.GetAll<TypedEntity>().ToArray();

                    // Create a tree node for each artist returned from the provider, and loop over them
                    foreach (var treeNode in items.Select(item =>
                                (TreeNode)CreateTreeNode(
                                    item.Id,
                                    queryStrings,
                                    item.Attributes["Name"].DynamicValue.ToString(),
                                    Url.GetEditorUrl("Edit", EditorControllerId, new { id = item.Id }))))
                    {
                        // Set the icon for each artist
                        treeNode.Icon = "tree-user";

                        // Artists don't have children (yet)
                        treeNode.HasChildren = false;

                        // Add the context menu items
                        treeNode.AddEditorMenuItem<Delete>(this, "deleteUrl", "Delete");

                        // Add the tree node to the collection
                        NodeCollection.Add(treeNode);
                    }
                }
            }
            else
            {
                throw new NotSupportedException("The artists tree does not support more than 1 level");
            }

            return UmbracoTree();
        }

        /// <summary>
        /// Creates the root node
        /// </summary>
        /// <param name="queryStrings"></param>
        /// <returns></returns>
        protected override TreeNode CreateRootNode(FormCollection queryStrings)
        {
            var n = base.CreateRootNode(queryStrings);
            n.AddEditorMenuItem<CreateItem>(this, "createUrl", "Create");
            n.AddMenuItem<Reload>();
            return n;
        }

    }
}
