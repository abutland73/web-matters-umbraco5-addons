﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model;

namespace WebMatters.UmbracoAddOns.MusicCatalogSurfaceControllers.Models
{
    public class ArtistListViewModel
    {
        public IEnumerable<TypedEntity> Artists { get; set; }
    }
}
