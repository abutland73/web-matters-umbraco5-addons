﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Cms.Web.Context;
using Umbraco.Cms.Web.Surface;
using Umbraco.Framework;
using Umbraco.Cms.Web.EmbeddedViewEngine;
using Umbraco.Hive;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity;
using Umbraco.Framework.Persistence.Model;
using WebMatters.UmbracoAddOns.MusicCatalogSurfaceControllers.Models;

namespace WebMatters.UmbracoAddOns.MusicCatalogSurfaceControllers.Controllers
{
    [Surface("D707DAE8-28BB-11E1-BDBA-CF134824019B")]
    public class MusicCatalogSurfaceController : SurfaceController 
    {
        public MusicCatalogSurfaceController(IRoutableRequestContext requestContext) : base(requestContext)
        {
        }

        [ChildActionOnly]
        public PartialViewResult DisplayArtistList()
        {
            var model = new ArtistListViewModel();
            var hive = RoutableRequestContext.Application.Hive.GetReader<IMusicCatalog>(new Uri("webmatters-music-catalog://"));
            using (var uow = hive.CreateReadonly())
            {
                model.Artists = uow.Repositories.GetAll<TypedEntity>();
            }
            return PartialView(EmbeddedViewPath.Create("WebMatters.UmbracoAddOns.MusicCatalogSurfaceControllers.Views.DisplayArtistList.cshtml,WebMatters.UmbracoAddOns.MusicCatalogSurfaceControllers"), model);
        }
    }
}
