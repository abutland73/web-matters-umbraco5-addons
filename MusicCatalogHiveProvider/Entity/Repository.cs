﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Umbraco.Framework;
using Umbraco.Framework.Context;
using Umbraco.Framework.Persistence;
using Umbraco.Framework.Persistence.Model;
using Umbraco.Framework.Persistence.Model.Associations;
using Umbraco.Framework.Persistence.Model.Associations._Revised;
using Umbraco.Framework.Persistence.Model.Attribution;
using Umbraco.Framework.Persistence.Model.Constants;
using Umbraco.Framework.Persistence.Model.Constants.AttributeDefinitions;
using Umbraco.Framework.Persistence.ProviderSupport._Revised;
using Umbraco.Hive.ProviderSupport;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.DataAccess;
using Umbraco.Framework.Linq.ResultBinding;
using Umbraco.Framework.Linq.QueryModel;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity
{
    public class Repository : AbstractEntityRepository
    {
        private IMusicCatalogRepository _catalogRepository;

        public Repository(ProviderMetadata providerMetadata, AbstractSchemaRepository schemas, IFrameworkContext frameworkContext)
            : base(providerMetadata, schemas, frameworkContext)
        { }

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this instance can read relations.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can read relations; otherwise, <c>false</c>.
        /// </value>
        public override bool CanReadRelations { get { return true; } }

        /// <summary>
        /// Gets a value indicating whether this instance can write relations.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can write relations; otherwise, <c>false</c>.
        /// </value>
        public override bool CanWriteRelations { get { return false; } }

        /// <summary>
        /// Gets/instantiates the actual repostitory for data access
        /// </summary>
        private IMusicCatalogRepository CatalogRepository { get { return _catalogRepository ?? (_catalogRepository = new SqlMusicCatalogRepository()); } }

        #endregion

        #region Entity

        /// <summary>
        /// Identifies if a <see cref="!:TEntity"/> with matching <paramref name="id"/> can be found in this repository.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam><param name="id">The id.</param>
        /// <returns>
        /// <code>
        /// true
        /// </code>
        ///  if the item with <paramref name="id"/> can be found, otherwise 
        /// <code>
        /// false
        /// </code>
        /// .
        /// </returns>
        public override bool Exists<TEntity>(HiveId id)
        {
            return Get<TEntity>(true, id).Any();
        }

        /// <summary>
        /// Performs the add or update.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected override void PerformAddOrUpdate(TypedEntity entity)
        {
            var artist = ConvertTypedEntityToArtist(entity);
            CatalogRepository.AddOrUpdateArtist(artist);
        }

        /// <summary>
        /// Performs the delete.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The id.</param>
        protected override void PerformDelete<T>(HiveId id)
        {
            CatalogRepository.DeleteArtist(GetArtistIdFromHiveId(id));
        }

        /// <summary>
        /// Performs the get.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="allOrNothing">if set to <c>true</c> [all or nothing].</param>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public override IEnumerable<T> PerformGet<T>(bool allOrNothing, params HiveId[] ids)
        {
            foreach (var hiveId in ids.Where(x => x.Value.ToString().StartsWith("mc-artist-")).Select(x => int.Parse(x.Value.ToString().Replace("mc-artist-", ""))))
            {
                var entity = ConvertArtistToTypedEntity(CatalogRepository.GetArtistById(hiveId));

                yield return entity as T;

            }
        }

        /// <summary>
        /// Performs the get all.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override IEnumerable<T> PerformGetAll<T>()
        {
            var artists = CatalogRepository.GetArtistList();
            foreach (var artist in artists)
            {
                yield return ConvertArtistToTypedEntity(artist) as T;
            }
        }

        /// <summary>
        /// Performs the execute many.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="objectBinder">The object binder.</param>
        /// <returns></returns>
        public override IEnumerable<T> PerformExecuteMany<T>(QueryDescription query, ObjectBinder objectBinder)
        {
            return Enumerable.Empty<T>();
        }

        /// <summary>
        /// Performs the execute scalar.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="objectBinder">The object binder.</param>
        /// <returns></returns>
        public override T PerformExecuteScalar<T>(QueryDescription query, ObjectBinder objectBinder)
        {
            return default(T);
        }

        /// <summary>
        /// Performs the execute single.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="objectBinder">The object binder.</param>
        /// <returns></returns>
        public override T PerformExecuteSingle<T>(QueryDescription query, ObjectBinder objectBinder)
        {
            return default(T);
        }
        
        #endregion

        #region Relations

        /// <summary>
        /// Adds the relation.
        /// </summary>
        /// <param name="item">The item.</param>
        protected override void PerformAddRelation(IReadonlyRelation<IRelatableEntity, IRelatableEntity> item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes the relation.
        /// </summary>
        /// <param name="item">The item.</param>
        protected override void PerformRemoveRelation(IRelationById item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Performs the get parent relations.
        /// </summary>
        /// <param name="childId">The child id.</param>
        /// <param name="relationType">Type of the relation.</param>
        /// <returns></returns>
        public override IEnumerable<IRelationById> PerformGetParentRelations(HiveId childId, RelationType relationType = null)
        {
            if (childId.EqualsIgnoringProviderId(FixedHiveIds.ContentVirtualRoot)) 
                yield break;

            yield return new RelationById(FixedHiveIds.ContentVirtualRoot, childId, FixedRelationTypes.ApplicationRelationType, 0);
        }

        /// <summary>
        /// Performs the get child relations.
        /// </summary>
        /// <param name="parentId">The parent id.</param>
        /// <param name="relationType">Type of the relation.</param>
        /// <returns></returns>
        public override IEnumerable<IRelationById> PerformGetChildRelations(HiveId parentId, RelationType relationType = null)
        {
            if (parentId == FixedHiveIds.ContentVirtualRoot)
            {
                foreach (var entity in GetAll<TypedEntity>())
                {
                    yield return new Relation(FixedRelationTypes.ApplicationRelationType, parentId, entity.Id, 0);
                }
            }
        }

        /// <summary>
        /// Performs the get ancestor relations.
        /// </summary>
        /// <param name="descendentId">The descendent id.</param>
        /// <param name="relationType">Type of the relation.</param>
        /// <returns></returns>
        public override IEnumerable<IRelationById> PerformGetAncestorRelations(HiveId descendentId, RelationType relationType = null)
        {
            return GetParentRelations(descendentId, relationType).SelectRecursive(x => GetParentRelations(x.SourceId, relationType));
        }

        /// <summary>
        /// Performs the get descendent relations.
        /// </summary>
        /// <param name="ancestorId">The ancestor id.</param>
        /// <param name="relationType">Type of the relation.</param>
        /// <returns></returns>
        public override IEnumerable<IRelationById> PerformGetDescendentRelations(HiveId ancestorId, RelationType relationType = null)
        {
            return GetChildRelations(ancestorId, relationType).SelectRecursive(x => GetChildRelations(x.DestinationId, relationType));
        }

        /// <summary>
        /// Performs the find relation.
        /// </summary>
        /// <param name="sourceId">The source id.</param>
        /// <param name="destinationId">The destination id.</param>
        /// <param name="relationType">Type of the relation.</param>
        /// <returns></returns>
        public override IRelationById PerformFindRelation(HiveId sourceId, HiveId destinationId, RelationType relationType)
        {
            throw new NotImplementedException();
        }
        
        #endregion

        #region Helper Methods

        /// <summary>
        /// Gets the album from the typed entity
        /// </summary>
        /// <param name="artist">The artist.</param>
        /// <returns></returns>
        private static TypedEntity ConvertArtistToTypedEntity(Artist artist)
        {
            if (artist == null) 
                return null;

            var output = new TypedEntity()
            {
                Id = new HiveId(string.Format("mc-artist-{0}", artist.Id)),
                EntitySchema = new ArtistSchema(),
                UtcCreated = artist.CreatedOn,
                UtcModified = artist.LastModifiedOn,
                UtcStatusChanged = artist.LastModifiedOn,
            };

            var nameAttribute = new TypedAttribute(new NameAttributeDefinition(), artist.Name);
            nameAttribute.Values.Add("Name", artist.Name);
            nameAttribute.Values.Add("UrlName", artist.Name);
            nameAttribute.Id = new HiveId(nameAttribute.AttributeDefinition.Alias);
            output.Attributes.Add(nameAttribute);

            var descriptionAttribute = new TypedAttribute(new DescriptionAttributeDefinition(), artist.Description);
            descriptionAttribute.Values.Add("Description", artist.Description);
            descriptionAttribute.Id = new HiveId(descriptionAttribute.AttributeDefinition.Alias);
            output.Attributes.Add(descriptionAttribute);

            return output;
        }

        /// <summary>
        /// Gets the typed entity from the artist
        /// </summary>
        /// <param name="entity">The typed entity.</param>
        /// <returns></returns>
        private static Artist ConvertTypedEntityToArtist(TypedEntity entity)
        {
            if (entity == null)
                return null;

            var artist = new Artist()
            {
                Id = GetArtistIdFromHiveId(entity.Id),
                Name = entity.Attributes["Name"].DynamicValue.ToString(),
                Description = entity.Attributes["Description"].DynamicValue.ToString(),
            };

            return artist;
        }

        /// <summary>
        /// Extracts the artist ID from the TypedEntity's Hive Id
        /// </summary>
        /// <param name="id">The TypedEntity's Hive Id</param>
        /// <returns></returns>
        private static int GetArtistIdFromHiveId(HiveId id)
        {
            if (id.Value == HiveIdValue.Empty)
                return 0;
            else
                return int.Parse(id.Value.ToString().Substring(id.Value.ToString().IndexOf("mc-artist-")).Replace("mc-artist-", ""));
        }
        
        #endregion

        /// <summary>
        /// Handles the disposal of resources. Derived from abstract class <see cref="DisposableObject"/> which handles common required locking logic.
        /// </summary>
        protected override void DisposeResources()
        {
            return;
        }
    }
}
