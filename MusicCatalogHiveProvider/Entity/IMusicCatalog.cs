﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Hive.ProviderGrouping;
using Umbraco.Hive.RepositoryTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity
{
    [RepositoryType("webmatters-music-catalog://")]
    public interface IMusicCatalog : IProviderTypeFilter
    { }
}
 