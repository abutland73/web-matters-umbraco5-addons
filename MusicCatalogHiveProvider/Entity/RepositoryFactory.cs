﻿using Umbraco.Framework;
using Umbraco.Framework.Context;
using Umbraco.Framework.Persistence.Model;
using Umbraco.Framework.Persistence.ProviderSupport._Revised;
using Umbraco.Hive.ProviderSupport;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity
{
    [DemandsDependencies(typeof(DemandBuilder))]
    public class RepositoryFactory : AbstractEntityRepositoryFactory 
    {
        public RepositoryFactory(ProviderMetadata providerMetadata, AbstractRevisionRepositoryFactory<TypedEntity> revisionSessionFactory, AbstractSchemaRepositoryFactory schemaSessionFactory, IFrameworkContext frameworkContext, ProviderDependencyHelper dependencyHelper) 
            : base(providerMetadata, revisionSessionFactory, schemaSessionFactory, frameworkContext, dependencyHelper)
        { }

        /// <summary>
        /// Gets an <see cref="AbstractReadonlyEntityRepository"/>. It will have only read operations.
        /// </summary>
        /// <returns></returns>
        public override AbstractReadonlyEntityRepository GetReadonlyRepository()
        {
            return GetRepository();
        }

        /// <summary>
        /// Gets the session from the factory.
        /// </summary>
        /// <returns></returns>
        public override AbstractEntityRepository GetRepository()
        {
            return new Repository(ProviderMetadata, SchemaRepositoryFactory.GetRepository(), FrameworkContext);
        }

        /// <summary>
        /// Handles the disposal of resources. Derived from abstract class <see cref="DisposableObject"/> which handles common required locking logic.
        /// </summary>
        protected override void DisposeResources()
        {
            return;
        }
    }
}
