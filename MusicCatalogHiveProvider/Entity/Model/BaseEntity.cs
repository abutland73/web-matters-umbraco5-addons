﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}
