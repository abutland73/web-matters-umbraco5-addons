﻿using System;
using System.Collections.Generic;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model
{
    public class Artist : BaseEntity
    {
        public string Description { get; set; }
    }
}