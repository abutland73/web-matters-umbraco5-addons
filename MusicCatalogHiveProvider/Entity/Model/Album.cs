﻿using System.Collections.Generic;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model
{   
    public class Album : BaseEntity {

        public decimal Price { get; set; }
        public Artist Artist { get; set; }
        public IList<Tag> Tags { get; set; }
    }
}
