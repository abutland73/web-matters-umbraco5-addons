﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model;
using System.Data.SqlClient;
using System.Data;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.DataAccess
{
    public class SqlMusicCatalogRepository : IMusicCatalogRepository
    {
        private const string _connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=umbracohivetest;User ID=umbraco5;password=xxxxxxxx";

        public SqlMusicCatalogRepository()
        {
        }

        /// <summary>
        /// Get list of artists
        /// </summary>
        /// <returns>List of Artist objects</returns>
        public IList<Artist> GetArtistList()
        {
            var artists = new List<Artist>();
            using (var cn = new SqlConnection(_connectionString))
            {
                var cm = cn.CreateCommand();
                cm.CommandType = CommandType.Text;
                cm.CommandText = "SELECT ID,Name,Description,CreatedOn,LastModifiedOn FROM Artists ORDER BY Name";
                cn.Open();
                using (var dr = cm.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dr.Read())
                    {
                        artists.Add(LoadArtist(dr));
                    }
                    dr.Close();
                }
            }
            return artists;
        }

        /// <summary>
        /// Helper method to load artist details from data reader
        /// </summary>
        /// <param name="dr">Date reader containing details of a single Artist</param>
        /// <returns>Single Artist object</returns>
        private Artist LoadArtist(IDataReader dr)
        {
            var artist = new Artist
            {
                Id = dr.GetInt32(0),
                Name = dr.GetString(1),
                Description = dr.GetString(2),
                CreatedOn = dr.GetDateTime(3),
                LastModifiedOn = dr.GetDateTime(4),
            };
            return artist;
        }

        /// <summary>
        /// Gets a single artist by Id
        /// </summary>
        /// <param name="id">The artist Id</param>
        /// <returns>Single artist object</returns>
        public Artist GetArtistById(int id)
        {
            Artist artist = null;
            using (var cn = new SqlConnection(_connectionString))
            {
                var cm = cn.CreateCommand();
                cm.CommandType = CommandType.Text;
                cm.CommandText = "SELECT ID, Name, Description, CreatedOn, LastModifiedOn FROM Artists WHERE ID = @artist";
                cm.Parameters.AddWithValue("@artist", id);
                cn.Open();
                using (var dr = cm.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dr.Read())
                    {
                        artist = LoadArtist(dr);
                    }
                    dr.Close();
                }
            }
            return artist;
        }

        /// <summary>
        /// Adds or updates an artist record
        /// </summary>
        /// <param name="artist">Artist object to persist</param>
        /// <remarks>Insert rather than update detected with artist.Id equal to zero</remarks>
        public void AddOrUpdateArtist(Artist artist)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cm = cn.CreateCommand();
                cm.CommandType = CommandType.Text;
                if (artist.Id == 0)
                {
                    cm.CommandText = "INSERT INTO Artists (Name, Description, CreatedOn, LastModifiedOn) VALUES (@name, @description, GetDate(), GetDate())";
                }
                else
                {
                    cm.CommandText = "UPDATE Artists SET Name = @name, Description = @description, LastModifiedOn = GetDate() WHERE ID = @id";
                    cm.Parameters.AddWithValue("@id", artist.Id);
                }
                cm.Parameters.AddWithValue("@name", artist.Name);
                cm.Parameters.AddWithValue("@description", artist.Description);
                cn.Open();
                cm.ExecuteNonQuery();
                cn.Close();
            }
        }

        /// <summary>
        /// Deletes an artist
        /// </summary>
        /// <param name="id">The artist Id</param>
        public void DeleteArtist(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cm = cn.CreateCommand();
                cm.CommandType = CommandType.Text;
                cm.CommandText = "DELETE FROM Artists WHERE ID = @id";
                cm.Parameters.AddWithValue("@id", id);
                cn.Open();
                cm.ExecuteNonQuery();
                cn.Close();
            }          
        }
    }
}
