﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.DataAccess
{
    public interface IMusicCatalogRepository
    {
        IList<Artist> GetArtistList();
        Artist GetArtistById(int id);
        void AddOrUpdateArtist(Artist artist);        
        void DeleteArtist(int id);        
    }
}
