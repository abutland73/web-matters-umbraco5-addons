﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.Model;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.DataAccess
{
    public class FakeMusicCatalogRepository : IMusicCatalogRepository
    {
        private IList<Artist> _artists;

        public FakeMusicCatalogRepository()
        {
            _artists = new List<Artist>
            {
                new Artist { Id = 1, Name = "Radiohead", Description = "Radiohead are an English rock band from Abingdon, Oxfordshire, formed in 1985. The band consists of Thom Yorke (vocals, guitars, keyboards), Jonny Greenwood (guitars, keyboards, other instruments), Ed O'Brien (guitars, backing vocals), Colin Greenwood (bass) and Phil Selway (drums, percussion).",
                    CreatedOn = new DateTime(2011, 1, 1, 0, 0, 0), LastModifiedOn = new DateTime(2011, 11, 5, 10, 0, 0)},
                new Artist { Id = 2, Name = "Flaming Lips", Description = "The Flaming Lips are an American alternative rock band, formed in Oklahoma City, Oklahoma in 1983.  Melodically, their sound contains lush, multi-layered, psychedelic rock arrangements, but lyrically their compositions show elements of space rock, including unusual song and album titles.",
                    CreatedOn = new DateTime(2011, 1, 1, 0, 0, 0), LastModifiedOn = new DateTime(2011, 11, 5, 10, 0, 0)},
                new Artist { Id = 3, Name = "Eels", Description = "Eels is an American indie rock band formed by singer/songwriter Mark Oliver Everett, better known as E. Other members rotate frequently, both in the studio and on stage.",
                    CreatedOn = new DateTime(2011, 1, 1, 0, 0, 0), LastModifiedOn = new DateTime(2011, 11, 5, 10, 0, 0)},
            };
        }
        public IList<Artist> GetArtistList()
        {
            return _artists;
        }

        public Artist GetArtistById(int id)
        {
            return _artists.Where(a => a.Id == id).SingleOrDefault();
        }

        public void AddOrUpdateArtist(Artist artist)
        {
        }

        public void DeleteArtist(int id)
        {            
        }
    }
}
