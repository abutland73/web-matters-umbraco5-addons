﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model;
using Umbraco.Hive.ProviderGrouping;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity
{
    public static class RepositoryExtensions
    {
        public static IEnumerable<TypedEntity> GetAllArtists(this IReadonlyEntityRepositoryGroup<IMusicCatalog> session)
        {
            return session.GetAll<TypedEntity>()
                .Where(x => x.EntitySchema.Alias == ArtistSchema.SchemaAlias);
        }

        public static IEnumerable<TypedEntity> GetAllProductsInCategory(this IReadonlyEntityRepositoryGroup<IMusicCatalog> session, int artistId)
        {
            return session.GetAll<TypedEntity>()
                .Where(x => x.EntitySchema.Alias == AlbumSchema.SchemaAlias && x.Attributes[ArtistIdAttributeDefinition.AliasValue].DynamicValue == artistId);
        }
    }
}
