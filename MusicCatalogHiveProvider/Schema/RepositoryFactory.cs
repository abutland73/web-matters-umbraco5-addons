using Umbraco.Framework;
using Umbraco.Framework.Context;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.ProviderSupport._Revised;
using Umbraco.Hive.ProviderSupport;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema
{
    public class RepositoryFactory : AbstractSchemaRepositoryFactory 
    {
        public RepositoryFactory(ProviderMetadata providerMetadata, IFrameworkContext frameworkContext, ProviderDependencyHelper dependencyHelper) 
            : base(providerMetadata, new NullProviderRevisionRepositoryFactory<EntitySchema>(providerMetadata, frameworkContext), frameworkContext, dependencyHelper)
        { }

		/// <summary>
		/// Gets an <see cref="AbstractReadonlySchemaRepository"/>. It will have only read operations.
		/// </summary>
		/// <returns></returns>
        public override AbstractReadonlySchemaRepository GetReadonlyRepository()
		{
            return GetRepository();
		}

        /// <summary>
        /// Gets an <see cref="AbstractSchemaRepository"/>.
        /// </summary>
        /// <returns></returns>
        public override AbstractSchemaRepository GetRepository()
        {
            return new Repository(ProviderMetadata, FrameworkContext);
        }

        /// <summary>
        /// Handles the disposal of resources. Derived from abstract class <see cref="DisposableObject"/> which handles common required locking logic.
        /// </summary>
        protected override void DisposeResources()
        {
            return;
        }
    }
}