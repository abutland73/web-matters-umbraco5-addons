﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Constants.SerializationTypes;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes {

    public class ReadOnlyAttributeType : AttributeType {

        public const string AliasValue = "mc-readonly";

        public ReadOnlyAttributeType()
            : base(
                AliasValue,
                AliasValue,
                "This type represents a readonly field",
                new StringSerializationType()) {
            Id = new HiveId("mc-readonly".EncodeAsGuid());
            RenderTypeProvider = "6A1F4266-E3A6-4BC1-8B79-81426CBAD9F1";
        }
    }
}
