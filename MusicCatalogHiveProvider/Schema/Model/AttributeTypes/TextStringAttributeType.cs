﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.Model.Constants.SerializationTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes
{
    public class TextStringAttributeType : AttributeType
    {
        public const string AliasValue = "mc-text-string";

        public TextStringAttributeType()
            : base(
            AliasValue,
            AliasValue,
            "This type represents internal system text",
            new StringSerializationType())
        {
            Id = new HiveId(AliasValue.EncodeAsGuid());
            RenderTypeProvider = "3F5ED845-7018-4BDE-AB4E-C7106EE0992D";
            RenderTypeProviderConfig = @"
<preValues>
    <preValue name='Mode'><![CDATA[SingleLine]]></preValue>
</preValues>";
        }
    }
}
