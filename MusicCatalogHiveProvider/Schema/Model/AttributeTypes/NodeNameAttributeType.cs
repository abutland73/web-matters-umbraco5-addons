﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.Model.Constants;
using Umbraco.Framework.Persistence.Model.Constants.SerializationTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes
{
    public class NodeNameAttributeType : AttributeType
    {
        public const string AliasValue = "system-node-name-type";

        public NodeNameAttributeType()
            : base(
                AliasValue,
                AliasValue,
                "This type represents the NodeName",
                new StringSerializationType())
        {
            Id = FixedHiveIds.NodeNameAttributeTypeId;
            RenderTypeProvider = "2B8ABB59-9474-457A-B198-044B86F43027";
        }
    }
}
