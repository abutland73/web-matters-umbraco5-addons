﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Constants.SerializationTypes;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes {
   
    public class NumericAttributeType : AttributeType {

        public const string AliasValue = "mc-numeric";

        public NumericAttributeType()
            : base(
                AliasValue,
                AliasValue,
                "This type represents a decimal, 2 decimal places",
                new StringSerializationType())
        {
            Id = new HiveId("decimal-pe".EncodeAsGuid());
            RenderTypeProvider = "81D5E0DF-5AF8-40C5-A171-FE2BB9FFA715";
            RenderTypeProviderConfig = @"
<preValues>
    <preValue name='DecimalPlaces'><![CDATA[2]]></preValue>
</preValues>";
        }
    }
}
