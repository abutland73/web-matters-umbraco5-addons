﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.Model.Constants.SerializationTypes;
using Umbraco.Framework;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes {
    
    public class ArtistDropdownAttributeType : AttributeType {

        public const string AliasValue = "mc-artist-dropdown";

        public ArtistDropdownAttributeType() 
            : base(
                AliasValue, 
                AliasValue, 
                "This type represents a music catalog artist dropdown", 
                new StringSerializationType()) {
                    Id = new HiveId("mc-artist-dropdown".EncodeAsGuid());
                    RenderTypeProvider = "BE8F4CA6-7CD3-433B-A04E-43250061E109";
                }
    }
}
