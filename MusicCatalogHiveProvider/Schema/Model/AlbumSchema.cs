﻿using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.Model.Constants.AttributeDefinitions;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model
{
    public class AlbumSchema : EntitySchema
    {
        public const string SchemaAlias = "mc-album-schema";

        public AlbumSchema()
            : base(SchemaAlias, "An Album")
        {
            Id = new HiveId(2);
            AttributeDefinitions.Add(new NameAttributeDefinition());
            AttributeDefinitions.Add(new SelectedTemplateAttributeDefinition());

            AttributeDefinitions.Add(new AlbumIdAttributeDefinition());
            AttributeDefinitions.Add(new ArtistIdAttributeDefinition());
            AttributeDefinitions.Add(new AlbumNameAttributeDefinition());
            AttributeDefinitions.Add(new PriceAttributeDefinition());
        }
    }
}
