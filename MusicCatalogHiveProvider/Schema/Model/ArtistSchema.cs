﻿using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.Model.Constants.AttributeDefinitions;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model
{
    public class ArtistSchema : EntitySchema
    {
        public const string SchemaAlias = "mc-artist-schema";

        public ArtistSchema()
            : base(SchemaAlias, "An Artist")
        {
            Id = new HiveId("AC1EF6D0-0F8A-11E1-B0F4-3E884824019B");
            AttributeDefinitions.Add(new NameAttributeDefinition());
            AttributeDefinitions.Add(new DescriptionAttributeDefinition());
        }
    }
}
