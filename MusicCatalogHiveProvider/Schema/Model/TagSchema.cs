﻿using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework.Persistence.Model.Constants.AttributeDefinitions;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model
{
    public class TagSchema : EntitySchema
    {
        public const string SchemaAlias = "mc-tag-schema";

        public TagSchema()
            : base(SchemaAlias, "A Tag")
        {
            Id = new HiveId(3);
            AttributeDefinitions.Add(new NameAttributeDefinition());
            AttributeDefinitions.Add(new SelectedTemplateAttributeDefinition());

            AttributeDefinitions.Add(new AttributeDefinition("mc-tag-name", "Tag Name") {
                AttributeGroup = new TagAttributeGroup(),
                AttributeType = new TextStringAttributeType()
            });
        }
    }
}
