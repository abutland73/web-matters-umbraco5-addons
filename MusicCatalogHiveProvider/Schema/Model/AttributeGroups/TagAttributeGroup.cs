using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups
{
    public class TagAttributeGroup : AttributeGroup
    {
        public TagAttributeGroup()
            : base("mc-tag-group", "Artist", 0)
        {
            Id = new HiveId("mc-tag-group");
        }
    }
}