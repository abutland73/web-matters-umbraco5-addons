using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups
{
    public class AlbumAttributeGroup : AttributeGroup
    {
        public AlbumAttributeGroup()
            : base("mc-album-group", "Album", 0)
        {
            Id = new HiveId("mc-album-group");
        }
    }
}