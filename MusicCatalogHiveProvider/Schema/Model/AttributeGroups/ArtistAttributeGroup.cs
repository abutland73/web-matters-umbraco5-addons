using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups
{
    public class ArtistAttributeGroup : AttributeGroup
    {
        public ArtistAttributeGroup()
            : base("mc-artist-group", "Artist", 0)
        {
            Id = new HiveId("mc-artist-group");
        }
    }
}