﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions {

    public class DescriptionAttributeDefinition : AttributeDefinition {

        public const string AliasValue = "Description";

        public DescriptionAttributeDefinition()
            : base(AliasValue, "Description")
        {
            Id = new HiveId(AliasValue);
            AttributeGroup = new ArtistAttributeGroup();
            AttributeType = new TextStringAttributeType();
        }
    }
}