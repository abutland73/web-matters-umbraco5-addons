﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions
{
    public class AlbumIdAttributeDefinition : AttributeDefinition
    {
        public const string AliasValue = "mc-album-id";

        public AlbumIdAttributeDefinition()
            : base(AliasValue, "Album Id")
        {
            Id = new HiveId(AliasValue);
            AttributeGroup = new AlbumAttributeGroup();
            AttributeType = new ReadOnlyAttributeType();
        }
    }
}