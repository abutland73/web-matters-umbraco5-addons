﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions {

    public class PriceAttributeDefinition : AttributeDefinition {

        public const string AliasValue = "mc-album-price";

        public PriceAttributeDefinition()
            : base(AliasValue, "Album Price") {
            Id = new HiveId(AliasValue);
            AttributeGroup = new AlbumAttributeGroup();
            AttributeType = new NumericAttributeType();
        }
    }
}