﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework.Persistence.Model.Attribution.MetaData;
using Umbraco.Framework;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeGroups;
using WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeTypes;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.Model.AttributeDefinitions {

    public class NameAttributeDefinition : AttributeDefinition {

        public const string AliasValue = "Name";

        public NameAttributeDefinition()
            : base(AliasValue, "Name")
        {
            Id = new HiveId(AliasValue);
            AttributeGroup = new ArtistAttributeGroup();
            AttributeType = new TextStringAttributeType();
        }
    }
}