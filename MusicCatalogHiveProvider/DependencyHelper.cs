﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Framework;
using Umbraco.Framework.Persistence.ProviderSupport._Revised;
using Umbraco.Hive.ProviderSupport;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider
{
    public class DependencyHelper : ProviderDependencyHelper
    {
        public DependencyHelper(ProviderMetadata providerMetadata) 
            : base(providerMetadata)
        { }

        /// <summary>
        /// Handles the disposal of resources. Derived from abstract class <see cref="DisposableObject"/> which handles common required locking logic.
        /// </summary>
        protected override void DisposeResources()
        {
            return;
        }
    }
}
