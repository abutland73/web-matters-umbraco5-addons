﻿using System;
using Umbraco.Framework;
using Umbraco.Framework.DependencyManagement;
using Umbraco.Hive.ProviderSupport;
using Umbraco.Hive;

namespace WebMatters.UmbracoAddOns.MusicCatalogHiveProvider
{
    public class DemandBuilder : AbstractProviderDependencyBuilder
    {
        public override void Build(IContainerBuilder containerBuilder, IBuilderContext context)
        {
            Mandate.ParameterNotNull(containerBuilder, "containerBuilder");
            Mandate.ParameterNotNull(context, "context");
        }

        /// <summary>
        /// Initialises the specified builder context.
        /// </summary>
        /// <param name="builderContext">The builder context.</param>
        public override void Initialise(IBuilderContext builderContext)
        {
            return;
        }

        /// <summary>
        /// Gets the provider bootstrapper factory.
        /// </summary>
        /// <param name="builderContext">The builder context.</param>
        /// <returns></returns>
        public override Func<IResolutionContext, AbstractProviderBootstrapper> GetProviderBootstrapperFactory(IBuilderContext builderContext)
        {
            return null;
        }

        /// <summary>
        /// Gets the provider dependency helper factory.
        /// </summary>
        /// <param name="builderContext">The builder context.</param>
        /// <returns></returns>
        public override Func<IResolutionContext, ProviderDependencyHelper> GetProviderDependencyHelperFactory(IBuilderContext builderContext)
        {
            return null;
        }
    }
}
