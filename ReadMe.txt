Umbraco 5 Add-Ons
=================
This is an exploratory project to create a custom hive provider for Umbraco 5 with a third party database.

The solution consists of 3 projects:
 - Hive Provider
 - Custom Tree for rendering the list of items in the back office
 - Custom Editor plugin for editing the list

Database
--------
The custom database is very simple - just a single table at this point created with the following script:

CREATE TABLE [dbo].[Artists](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[CreatedOn] [smalldatetime] NOT NULL,
	[LastModifiedOn] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Artists] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO Artists (Name,Description,CreatedOn,LastModifiedOn)
VALUES ('Radiohead','Radiohead are an English rock band from Abingdon, Oxfordshire, formed in 1985. The band consists of Thom Yorke (vocals, guitars, keyboards), Jonny Greenwood (guitars, keyboards, other instruments), Ed O''Brien (guitars, backing vocals), Colin Greenwood (bass) and Phil Selway (drums, percussion).',GetDate(),GetDate())
INSERT INTO Artists (Name,Description,CreatedOn,LastModifiedOn)
VALUES ('Flaming Lips','The Flaming Lips are an American alternative rock band, formed in Oklahoma City, Oklahoma in 1983.  Melodically, their sound contains lush, multi-layered, psychedelic rock arrangements, but lyrically their compositions show elements of space rock, including unusual song and album titles.',GetDate(),GetDate())
INSERT INTO Artists (Name,Description,CreatedOn,LastModifiedOn)
VALUES ('Eels','Eels is an American indie rock band formed by singer/songwriter Mark Oliver Everett, better known as E. Other members rotate frequently, both in the studio and on stage.',GetDate(),GetDate())
GO

The connection string is hardcoded within the SqlMusicCatalogRepository.cs file.

Configuration
-------------
In order to configure the Umbraco instance to use the hive provider, the following additional lines are required within the umbraco.hive.config file:

<hive>
  <available-providers>
    <writers>
      <add key="webmatters-music-catalog" type="WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Entity.RepositoryFactory, WebMatters.UmbracoAddOns.MusicCatalogHiveProvider" providerConfig="">
        <schema type="WebMatters.UmbracoAddOns.MusicCatalogHiveProvider.Schema.RepositoryFactory, WebMatters.UmbracoAddOns.MusicCatalogHiveProvider" providerConfig="" />
      </add>	  
    </writers>
  </available-providers>

  <provider-groups>
	<group key="music-catalog">
		<readers>
			<use provider="webmatters-music-catalog" ordinal="0" />
		</readers>
		<read-writers>
			<use provider="webmatters-music-catalog" ordinal="0" isPassthrough="false" />
		</read-writers>
		<uri-matches>
			<match uri="webmatters-music-catalog://" />
		</uri-matches>
	</group>	

  </provider-groups>
</hive>

And similar to configure the tree, the following is needed within umbraco.cms.trees.config:

<trees>
  <add application="content" controllerType="WebMatters.UmbracoAddOns.MusicCatalogTrees.MusicCatalogTreeController, WebMatters.UmbracoAddOns.MusicCatalogTrees" />
</trees>


More Information and Feedback
-----------------------------
See: http://web-matters.blogspot.com/2011/11/umbraco-5-hive-provider-tree-editor.html
